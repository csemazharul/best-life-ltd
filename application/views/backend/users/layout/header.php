<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codervent.com/dashrock/color-admin/authentication-signup.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Feb 2019 04:23:10 GMT -->
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
	<meta name="description" content=""/>
	<meta name="author" content=""/>
	<title>DashRock - Bootstrap 4 Admin Dashboard Template</title>
	<!--favicon-->
	<link rel="icon" href="<?php echo base_url(); ?>ui/backend/images/favicon.ico" type="image/x-icon">
	<!-- Bootstrap core CSS-->
	<link href="<?php echo base_url(); ?>ui/backend/css/bootstrap.min.css" rel="stylesheet"/>
	<!-- animate CSS-->
	<link href="<?php echo base_url(); ?>ui/backend/css/animate.css" rel="stylesheet" type="text/css"/>
	<!-- Icons CSS-->
	<link href="<?php echo base_url(); ?>ui/backend/css/icons.css" rel="stylesheet" type="text/css"/>
	<!-- Custom Style-->
	<link href="<?php echo base_url(); ?>ui/backend/css/app-style.css" rel="stylesheet"/>

</head>

<body class="authentication-bg">
