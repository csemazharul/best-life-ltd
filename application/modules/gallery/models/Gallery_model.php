<?php


class Gallery_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function galleryInsert($gallery)
	{

		$this->db->insert('galleries', $gallery);

	}
	public function getAllgallery()
	{
		$query = $this->db->get('galleries');
		return $query->result();
	}

	public function getGallery($id)
	{
		$query = $this->db->get_where('galleries',array('id'=>$id));
		return $query->row_array();
	}

	public function galleryUpdate($gallery, $id)
	{
		$this->db->where('galleries.id', $id);
		return $this->db->update('galleries', $gallery);
	}

	public function deleteGallery($id)
	{
		$this->db->where('galleries.id', $id);
		return $this->db->delete('galleries');
	}


}
