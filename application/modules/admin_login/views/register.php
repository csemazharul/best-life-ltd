<?php
$this->load->view('backend/users/layout/header');
?>
<div id="wrapper">
	<div class="card card-authentication1 mx-auto my-3 animated zoomIn">
		<div class="card-body">
			<div class="card-content p-2">
				<div class="text-center">
					<img src="<?php echo base_url(); ?>ui/backend/images/logo-icon.png"/>

					<h2><?php echo $this->session->flashdata('message'); ?></h2>

				</div>



				<div class="card-title text-uppercase text-center py-2">Sign Up</div>
				<form method="post" action="<?php echo base_url(''); ?>register/store">
					<div class="form-group">
						<div class="position-relative has-icon-left">
							<label for="exampleInputName" class="sr-only">Name</label>
							<input type="text" name="user_name" <?php echo set_value('user_name'); ?> id="exampleInputName" class="form-control" placeholder="Name">
							<span id="input-14-error" class="error"><?php echo form_error('user_name'); ?></span>
							<div class="form-control-position">
								<i class="icon-user"></i>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="position-relative has-icon-left">
							<label for="exampleInputEmailId" class="sr-only">Email ID</label>
							<input type="text" id="user_email" name="user_email" class="form-control" placeholder="Email ID">
							<span id="input-14-error" class="error"><?php echo form_error('user_email'); ?></span>
							<div class="form-control-position">
								<i class="icon-envelope-open"></i>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="position-relative has-icon-left">
							<label for="exampleInputPassword" class="sr-only">Password</label>
							<input type="password" id="user_password" name="user_password" class="form-control" placeholder="Password">
							<span id="input-14-error" class="error"><?php echo form_error('user_password'); ?></span>
							<div class="form-control-position">
								<i class="icon-lock"></i>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="position-relative has-icon-left">
							<label for="exampleInputRetryPassword" class="sr-only">Retry Password</label>
							<input type="password" id="confirm_password" name="confirm_password" class="form-control" placeholder="Retry Password">
							<span id="input-14-error" class="error"><?php echo form_error('confirm_password'); ?></span>
							<div class="form-control-position">
								<i class="icon-lock"></i>
							</div>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-danger shadow-danger btn-block waves-effect waves-light">Sign Up</button>
					</div>

					<div class="form-group text-center">
						<p class="text-muted">Already have an account? <a href="<?php echo base_url(); ?>login"> Sign In here</a></p>
					</div>
					<div class="form-group text-center">
						<hr>
						<h5>OR</h5>
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-facebook shadow-facebook text-white btn-block waves-effect waves-light"><i class="fa fa-facebook-square"></i> Sign In With Facebook</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!--Start Back To Top Button-->
	<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
	<!--End Back To Top Button-->
</div><!--wrapper-->
<?php
$this->load->view('backend/users/layout/footer');
?>
