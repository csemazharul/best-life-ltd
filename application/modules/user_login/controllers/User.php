<?php


class User extends MX_Controller
{
	public function __construct(){

		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->model('user_model');
		$this->load->library('session');
		$this->load->library('form_validation');
	}

	public function register()
	{
			$this->load->view('/register');

	}

	public function register_user()
	{
		$data = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules('email', 'email id', 'required|is_unique[login.email]');
		$this->form_validation->set_rules('country_code', 'country code', 'required');
		$this->form_validation->set_rules('mobile_no', 'mobile no', 'required');
		$this->form_validation->set_rules('firstname', 'first name', 'required');
		$this->form_validation->set_rules('lastname', 'last name', 'required');
		$this->form_validation->set_rules('date_of_birth', 'date of birth', 'required');
		$this->form_validation->set_rules('currency', 'currency', 'required');
		$this->form_validation->set_rules('fullname', 'fullname', 'required');
		$this->form_validation->set_rules('user_id', 'user id', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('confirm_password', 'confirm password', 'required|matches[password]');
		$this->form_validation->set_rules('t_pin', 't pin', 'required');
		$this->form_validation->set_rules('tre_pin', 'tre_pin', 'required');


		if ($this->form_validation->run())
		{
			$data['success'] = true;
			$user=array(
				'email'=>$this->input->post('email'),
				'country_code'=>$this->input->post('country_code'),
				'mobile_no'=>$this->input->post('mobile_no'),
				'firstname'=>$this->input->post('firstname'),
				'lastname'=>$this->input->post('lastname'),
				'date_of_birth'=>$this->input->post('date_of_birth'),
				'currency'=>$this->input->post('currency'),
				'fullname'=>$this->input->post('fullname'),
				'user_id'=>$this->input->post('user_id'),
				'password'=>md5($this->input->post('password')),
				't_pin'=>$this->input->post('t_pin'),
				'tre_pin'=>$this->input->post('tre_pin'),
				'created_at'=>date('Y-m-d h:i:a'),
				'updated_at'=>date('Y-m-d h:i:a'),
				'verify_status'=>md5(rand(1,100000)),
			);
			$this->user_model->register_user($user);
			$this->sendEmail($user['verify_status'],$user['email']);

			$this->session->set_flashdata('message', '<strong>Thank you!</strong>Your Mail has been sent successfully.</span>');
			$this->load->view('register');
			// $this->load->view("register.php");
		}

		else {

			foreach ($_POST as $key => $value)
			{
				$data['messages'][$key] = form_error($key);

			}
		}
		echo json_encode($data);

	}

	public function sendEmail($token,$email)
	{
		$this->load->library('email');
		$config = array();
		$config['protocol'] = 'mail';
		$config['smtp_host'] = 'mail.floricbd.com';
		$config['smtp_user'] = 'bestlifeinfo@floricbd.com';
		$config['smtp_pass'] = 'zC=xI2J^Nrie';
		$config['smtp_port'] = 26;
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->from('bestlifeinfo@floricbd.com', 'Best Life Limited');

		$this->email->to($email);
		$this->email->subject('Email Verification');
		$message = "
			 Please click this link to verify your account:
			 http://best_life_limited/verified/".$token;

		$this->email->message($message);

		$this->email->send();

	}
	public function verified()
	{
		$verified = $this->uri->segment(3);


		$check=$this->user_model->verified_check($verified);
		if($check)
		{
			$this->db->set('verify_status',1); //value that used to update column
			$this->db->where('verify_status', $verified); //which row want to upgrade
			$status=$this->db->update('login');  //table name
			if($status)
			{
				$this->session->set_flashdata('message', '<strong>Success!</strong>Your email verified..please login now.</span>');
				redirect('/user/login');
			}
		}
	}

	public function loginForm()
	{

		if(isset($_SESSION['email']))
		{
			redirect('admin');
		}
		else
		{

			$this->load->view('/login.php');

		}

	}

	function login_user(){
		$user_login=array(

			'email'=>$this->input->post('email'),
			'password'=>md5($this->input->post('password'))

		);
//$user_login['user_email'],$user_login['user_password']
		$data['users']=$this->user_model->login_user($user_login['email'],$user_login['password']);

		if($data['users'])
		{
			$this->session->set_userdata('email',$data['users'][0]['email']);
			$this->session->set_userdata('firstname',$data['users'][0]['firstname']);

			redirect('/admin', 'refresh');

		}
		else{
			$this->session->set_flashdata('message', 'Wrong Information please again try again.');
			$this->load->view("/login.php");

		}


	}

	public function user_logout(){

		$this->session->sess_destroy();
			$this->session->set_flashdata('message', 'Your account logout successfully.');
		$this->load->view("/login.php");
	}

}
