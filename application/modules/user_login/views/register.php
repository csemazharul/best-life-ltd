<?php
$this->load->view('frontend/layout/header');
?>
<div class="breadcrumb_wrapper">

		<div class="breadcrumb_block">
				<ul>
						<li><a href="index.html">home</a></li>
						<li>Registration</li>
				</ul>
		</div>
</div>

<!--Contact Form-->
<div class=" contact_form_wrapper clv_section">
		<div class="container">
				<div class="row">
						<div class="col-lg-8 col-md-8 offset-lg-2">
								<div class="contact_form_section" style="box-shadow:0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);border:none;">
										<div class="row">

											<?php if(isset($_SESSION['message']))
											{
											?>

												<div class="alert alert-success" style="margin-left:200px" id="message">
													 <?php echo $this->session->flashdata('message'); ?>

												</div>
											<?php } ?>
												<div class="col-md-12 col-lg-12">
														<h3>Registration</h3>

												</div>


					<form id="signupForm" action="<?php echo base_url() ?>user/registration/store" method="post">

											<div class="col-md-6 col-lg-6">
													<div class="form_block">
														<label for="country_code">Select Country</label>
															<select class="form-control" name="country_code" id="country_code">
																<option   disabled selected>choose Country</option>
																 <option value="USA">USA</option>
																 <option value="BAN">BAN</option>
																
															</select>

													</div>
											</div>
											<div class="col-md-6 col-lg-6">
													<div class="form_block">
															<label for="sponser_id">Sponcor ID</label>
															<input type="text" name="sponser_id" class="form_field require" placeholder="Sponsor ID" >
															<span id="input-14-error" class="" style="color:#d93025;"><?php echo form_error('sponser_id'); ?></span>
													</div>
											</div>
												<div class="col-md-6 col-lg-6">
														<div class="form_block">
															<label for="firstname">First Name</label>
																<input type="text" id="firstname" name="firstname" class="form-control form_field require" placeholder="First Name" >

														</div>
												</div>
												<div class="col-md-6 col-lg-6">
														<div class="form_block">
																<label for="lastname">Last Name</label>
																<input type="text" id="lastname" name="lastname" class="form_field require" placeholder="Last Name" >
																<span id="input-14-error" class="" style="color:#d93025;"><?php echo form_error('lastname'); ?></span>
														</div>
												</div>

												<div class="col-md-6 col-lg-6">
														<div class="form_block">
																<label for="date_of_birth">Date Of Birth</label>
																<input class="form-control" type="date" name="date_of_birth">
																<span id="input-14-error" class="" style="color:#d93025;"><?php echo form_error('date_of_birth'); ?></span>
														</div>
												</div>


												<div class="col-md-6 col-lg-6">
														<div class="form_block">
																<label for="corrency">select corrency</label>
																<select id="currency" name="currency" class='form-control form_field require'>
																	<option value="0" disabled selected="true">choose currency</option>
																	 <option value="USD">USD</option>
																	 <option value="EURO">EURO</option>
																	 <option value="BDT">BDT</option>
																</select>

																<span id="input-14-error" class="" style="color:#d93025;"><?php echo form_error('currency'); ?></span>
														</div>
												</div>

												<div class="col-md-6 col-lg-6">
														<div class="form_block">
																<label for="fullname">Full Name</label>
																<input type="text" id="fullname" name="fullname" class="form_field" placeholder="Full Name" >
																<span id="input-14-error" class="" style="color:#d93025;"><?php echo form_error('fullname'); ?></span>
														</div>
												</div>

												<div class="col-md-6 col-lg-6">
														<div class="form_block">
																<label for="user_id">User ID</label>
																<input type="text" id="user_id" name="user_id" class="form_field" placeholder="Last Name" >
																<span id="input-14-error" class="" style="color:#d93025;"><?php echo form_error('user_id'); ?></span>
														</div>
												</div>

												<div class="col-md-6 col-lg-6">
														<div class="form_block">
															<label for="password">Password</label>
																<input type="password" id="password" name="password"  class=" form_field require" placeholder="Password" data-valid="password" data-error="" >
																<span id="input-14-error" class="" style="color:#d93025;"><?php echo form_error('password'); ?></span>
														</div>
												</div>
												<div class="col-md-6 col-lg-6">
														<div class="form_block">
															<label for="c_password">Re-Password</label>
																<input type="password" id="c_password" name="confirm_password" class="form_field require" placeholder="Re-password" >
																<span id="input-14-error" class="" style="color:#d93025;"><?php echo form_error('confirm_password'); ?></span>
														</div>
												</div>

												<div class="col-md-6 col-lg-6">
														<div class="form_block">
															<label for="t_pin">T-Pin</label>
																<input type="text" id="t_pin" name="t_pin"  class=" form_field require" placeholder="T-Pin" data-valid="password" data-error="" >
																<span id="input-14-error" class="" style="color:#d93025;"><?php echo form_error('t_pin'); ?></span>
														</div>
												</div>
												<div class="col-md-6 col-lg-6">
														<div class="form_block">
															<label for="tre_pin">TRe-Pin</label>
																<input type="text" id="tre_pin" name="tre_pin" class="form_field require" placeholder="TRe-Pin" >
																<span id="input-14-error" class="" style="color:#d93025;"><?php echo form_error('tre_pin'); ?></span>
														</div>
												</div>

												<div class="col-md-6 col-lg-6">
														<div class="form_block">
															<label id="contact_no">Contact No</label>
																<input type="text" id="contact_no" name="mobile_no" class="form_field require" data-valid="number" placeholder="Contact No">
																<span id="input-14-error" class="" style="color:#d93025;"><?php echo form_error('mobile_no'); ?></span>
														</div>
												</div>

												<div class="col-md-6 col-lg-6">
														<div class="form_block">
															  <label for="email">Email</label>
																<input type="email" id="email" name="email"  class=" form_field require" placeholder="Email" data-valid="email" data-error="" >
																<span id="input-14-error1" class="" style="color:#d93025;"></span>
														</div>
												</div>

												<div class="col-md-12 col-lg-12">
														<div class="form_block">
																<button id='button' type="submit" class="clv_btn submitForm">Submit</button>
														</div>
												</div>
					</form>
										</div>
								</div>
						</div>

				</div>
		</div>
</div>

<script type='text/javascript'>
// var btn = document.getElementById("button");
// btn.addEventListener("click",function(e){
//     let email=document.getElementById("email").value;
// 		if(email=='')
// 		{
// 			console.log("fillup your email");
// 			e.preventDefault();
// 		}
//
// },false);



</script>
<?php
$this->load->view('frontend/layout/footer');
?>

<script>
// $("#signupForm" ).validate({
//     rules: {
// 			country_code: {
// 					required: true
// 			},
//         sponser_id: "required",
//         firstname: "required",
//         lastname: "required",
//         date_of_birth: "required",
//         currency: "required",
//         fullname: "required",
//         user_id: "required",
//         password: "required",
//         confirm_password: {
//             required:true,
//             equalTo:"#password"
//           },
//
//         t_pin: "required",
//         tre_pin: "required",
//         mobile_no: "required",
//         email: "required",
//
//
//     },messages: {
//
//         confirm_password:{
//             equalTo:"password doest not match.",
//         },
// 				country_code: {
//             required: "Please select your counry code",
//             },
//
//
//     },
//
//
//     errorElement: "span",
//
//     errorPlacement: function ( error, element ) {
//
//         // Add the `help-block` class to the error element
//         error.addClass( "help-block").css('color','#D93025');
//
//         if ( element.prop( "type" ) === "select" ) {
//
//             error.insertAfter( element.parent( "label" ) );
//         } else {
//             error.insertAfter( element);
//         }
//     },
//
//     submitHandler: function(form) {
//
//         form.submit();
//     }
// });

$('#signupForm').submit(function(e) {
	e.preventDefault();


	var me = $(this);

	// perform ajax
	$.ajax({
		url: me.attr('action'),
		type: 'post',
		data: me.serialize(),
		dataType: 'json',
		success: function(response) {

			if (response.success == true) {
				// if success we would show message
				// and also remove the error class
				$('#the-message').append('<div class="alert alert-success">' +
					'<span class="glyphicon glyphicon-ok"></span>' +
					' Data has been saved' +
					'</div>');
				$('.form-group').removeClass('has-error')
								.removeClass('has-success');
				$('.text-danger').remove();

				// reset the form
				me[0].reset();

				// close the message after seconds
				$('.alert-success').delay(500).show(10, function() {
					$(this).delay(3000).hide(10, function() {
						$(this).remove();
					});
				})
			}
			else {

				$.each(response.messages, function(key, value) {

					var element = $('#' + key);

					element.closest('div.form-group')
					.removeClass('has-error')
					.addClass(value.length > 0 ? 'has-error' : 'has-success')
					.find('.text-danger')
					.remove();

					element.after(value);
				});
			}
		}
	});
});
</script>
<!-- <script>
$('#button').click(function(e)
			{
					let country_code=$('#country_code').val();
					let currency=$('#currency').val();
					if(country_code=='')
					{
						    error.addClass( "help-block").css('color','#D93025');
							e.preventDefault();
					}
					if(currency=='')
					{
						error.addClass( "help-block").css('color','#D93025');
					 e.preventDefault();

					}

			});

</script> -->
