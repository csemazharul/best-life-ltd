
<?php
$this->load->view('frontend/layout/header');
?>


<!--Breadcrumb-->
<div class="breadcrumb_wrapper">

  <div class="breadcrumb_block">
    <ul>
      <li><a href="index.html">home</a></li>
      <li>gallery</li>
    </ul>
  </div>
</div>

<!--Gallery-->
<div class="clv_gallery_wrapper index_v4">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-6 col-md-6">
        <div class="clv_heading">
          <h3>our gallery</h3>
          <div class="clv_underline"><img src="<?php echo base_url('ui/frontend/images/')?>/underline3.png" alt="image" /></div>
          <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dole magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="gallery_slide">
                  <div class="gallery_grid">
                    <div class="gallery_grid_item">
                      <div class="gallery_image">
                        <img src="<?php echo base_url('ui/frontend/images/')?>/gallery1.jpg" alt="image" />
                        <div class="gallery_overlay">
                          <a href="<?php echo base_url('ui/frontend/images/')?>/gallery1_big.jpg" class="view_image"><span><i class="fa fa-search" aria-hidden="true"></i></span></a>
                        </div>
                      </div>
                    </div>
                    <div class="gallery_grid_item">
                      <div class="gallery_image">
                        <img src="<?php echo base_url('ui/frontend/images/')?>/gallery2.jpg" alt="image" />
                        <div class="gallery_overlay">
                          <a href="<?php echo base_url('ui/frontend/images/')?>/gallery2_big.jpg" class="view_image"><span><i class="fa fa-search" aria-hidden="true"></i></span></a>
                        </div>
                      </div>
                    </div>
                    <div class="gallery_grid_item">
                      <div class="gallery_image">
                        <img src="<?php echo base_url('ui/frontend/images/')?>/gallery3.jpg" alt="image" />
                        <div class="gallery_overlay">
                          <a href="<?php echo base_url('ui/frontend/images/')?>/gallery3_big.jpg" class="view_image"><span><i class="fa fa-search" aria-hidden="true"></i></span></a>
                        </div>
                      </div>
                    </div>
                    <div class="gallery_grid_item">
                      <div class="gallery_image">
                        <img src="<?php echo base_url('ui/frontend/images/')?>/gallery4.jpg" alt="image" />
                        <div class="gallery_overlay">
                          <a href="<?php echo base_url('ui/frontend/images/')?>/gallery4_big.jpg" class="view_image"><span><i class="fa fa-search" aria-hidden="true"></i></span></a>
                        </div>
                      </div>
                    </div>
                    <div class="gallery_grid_item">
                      <div class="gallery_image">
                        <img src="<?php echo base_url('ui/frontend/images/')?>/gallery5.jpg" alt="image" />
                        <div class="gallery_overlay">
                          <a href="<?php echo base_url('ui/frontend/images/')?>/gallery5_big.jpg" class="view_image"><span><i class="fa fa-search" aria-hidden="true"></i></span></a>
                        </div>
                      </div>
                    </div>
                    <div class="gallery_grid_item">
                      <div class="gallery_image">
                        <img src="<?php echo base_url('ui/frontend/images/')?>/gallery7.jpg" alt="image" />
                        <div class="gallery_overlay">
                          <a href="<?php echo base_url('ui/frontend/images/')?>/gallery7_big.jpg" class="view_image"><span><i class="fa fa-search" aria-hidden="true"></i></span></a>
                        </div>
                      </div>
                    </div>
                    <div class="gallery_grid_item">
                      <div class="gallery_image">
                        <img src="<?php echo base_url('ui/frontend/images/')?>/gallery6.jpg" alt="image" />
                        <div class="gallery_overlay">
                          <a href="<?php echo base_url('ui/frontend/images/')?>/gallery6_big.jpg" class="view_image"><span><i class="fa fa-search" aria-hidden="true"></i></span></a>
                        </div>
                      </div>
                    </div>
                    <div class="gallery_grid_item">
                      <div class="gallery_image">
                        <img src="<?php echo base_url('ui/frontend/images/')?>/gallery8.jpg" alt="image" />
                        <div class="gallery_overlay">
                          <a href="<?php echo base_url('ui/frontend/images/')?>/gallery8_big.jpg" class="view_image"><span><i class="fa fa-search" aria-hidden="true"></i></span></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
        </div>
        <div class="col-md-12">
          <div class="load_more_btn">
            <a href="javascript:;" class="clv_btn">view more</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
    <div class="#">
            <div class="container">

                <div class="clv_newsletter_wrapper" style="top: -34px;">
                    <div class="newsletter_text">
                        <h2>get update from <br/>anywhere</h2>
                        <h4>subscribe us to get more info</h4>
                    </div>
                    <div class="newsletter_field">
                        <h3>don't miss out on the good news!</h3>
                        <div class="newsletter_field_block">
                            <input type="text" placeholder="Enter Your Email Here" />
                            <a href="javascript:;">subscribe now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>



<?php
$this->load->view('frontend/layout/footer');
?>
