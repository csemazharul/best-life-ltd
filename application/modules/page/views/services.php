<?php
$this->load->view('frontend/layout/header');
?>

<!-- BANNER -->
<div class="section banner-page-2">
	<div class="content-wrap pos-relative">
		<div class="container">

			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12">

					<div class="mb-3">
						<div class="title-page">SERVICES</div>
					</div>
					<div class="mb-3">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb ">
								<li class="breadcrumb-item"><a href="index.html">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Services</li>
							</ol>
						</nav>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<!-- ABOUT -->
<div class="section">
	<div class="content-wrap">
		<div class="container">

			<div class="row">

				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading text-center">
						Hello! Welcome to Coxe Business
					</h2>
					<p class="subheading text-center mb-5">Awesome features we offer exclusive only</p>
				</div>

			</div>

			<div class="row">

				<!-- Item 1 -->
				<div class="col-sm-12 col-md-6 col-lg-4 mb-5">
					<div class="anim-media text-center">
						<div class="media-box mb-3">
							<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x400.jpg" alt="" class="img-fluid shadow-lg">
						</div>
						<h4 class="text-black">AWESOME DESIGN</h4>
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent.</p>
					</div>
				</div>

				<!-- Item 2 -->
				<div class="col-sm-12 col-md-6 col-lg-4 mb-5">
					<div class="anim-media text-center">
						<div class="media-box mb-3">
							<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x400.jpg" alt="" class="img-fluid shadow-lg">
						</div>
						<h4 class="text-black">EASY CUSTOMIZE</h4>
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent.</p>
					</div>
				</div>

				<!-- Item 3 -->
				<div class="col-sm-12 col-md-6 col-lg-4 mb-5">
					<div class="anim-media text-center">
						<div class="media-box mb-3">
							<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x400.jpg" alt="" class="img-fluid shadow-lg">
						</div>
						<h4 class="text-black">FAST PUBLISH</h4>
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent.</p>
					</div>
				</div>

				<!-- Item 4 -->
				<div class="col-sm-12 col-md-6 col-lg-4 mb-5">
					<div class="anim-media text-center">
						<div class="media-box mb-3">
							<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x400.jpg" alt="" class="img-fluid shadow-lg">
						</div>
						<h4 class="text-black">VIDEO SUPPORT</h4>
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent.</p>
					</div>
				</div>

				<!-- Item 5 -->
				<div class="col-sm-12 col-md-6 col-lg-4 mb-5">
					<div class="anim-media text-center">
						<div class="media-box mb-3">
							<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x400.jpg" alt="" class="img-fluid shadow-lg">
						</div>
						<h4 class="text-black">NO CODE</h4>
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent.</p>
					</div>
				</div>

				<!-- Item 6 -->
				<div class="col-sm-12 col-md-6 col-lg-4 mb-5">
					<div class="anim-media text-center">
						<div class="media-box mb-3">
							<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x400.jpg" alt="" class="img-fluid shadow-lg">
						</div>
						<h4 class="text-black">WELL DOCUMENTATION</h4>
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent.</p>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<!-- CTA -->
<div class="section bg-primary">
	<div class="content-wrap py-5">
		<div class="container">

			<div class="row align-items-center">
				<div class="col-sm-12 col-md-12">
					<div class="cta-1">
						<div class="body-text text-white mb-3">
							<h3 class="my-1">Grow Up Your Business With Coxe</h3>
							<p class="uk18 mb-0">We provide high standar clean website for your business solutions</p>
						</div>
						<div class="body-action mt-3">
							<a href="#" class="btn btn-secondary">PURCHASE NOW</a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<?php
$this->load->view('frontend/layout/footer');
?>
