<?php
$this->load->view('backend/layout/header');
?>
<?php print_r($page['name'])?>
<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header text-uppercase">Page Edit</div>
					<div class="card-body">

						<form method="post" action="<?php echo base_url()?>page/update/<?php echo $page['id']?>" enctype="multipart/form-data">

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Page Title</label>
								<div class="col-sm-9">
									<input type="text" value="<?php echo $page['title']?>" name="title" id="basic-input" class="form-control">
								</div>
							</div>

							<div class="form-group row">
								<label for="basic-textarea" class="col-sm-3 col-form-label">Description</label>
								<div class="col-sm-9">
									<textarea rows="8"  name="description" class="form-control" id="basic-textarea"><?php echo $page['description']?></textarea>
								</div>
							</div>

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Name</label>
								<div class="col-sm-9">
									<input type="text" value="<?php echo $page['name']?>" name="name" id="basic-input" class="form-control">
								</div>
							</div>

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Picture</label>
								<div class="col-sm-9">
									<input type="file" name="photo" id="basic-input" class="form-control">
								</div>
							</div>

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Page Name</label>
								<div class="col-sm-9">
									<input type="text" value="<?php echo $page['page_name']?>" name="page_name" id="basic-input" class="form-control">
								</div>
							</div>

							<button type="submit" class="btn btn btn-primary shadow btn-block ">Submit</button>

						</form>

					</div>
				</div>
			</div>
		</div><!--End Row-->
	</div>
	<!-- End container-fluid-->

</div><!--End content-wrapper

<?php
$this->load->view('backend/layout/footer');
?>
