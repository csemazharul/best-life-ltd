<?php
$this->load->view('backend/layout/header');
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header"><i class="fa fa-table"></i> Page List</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="default-datatable" class="table table-bordered">
								<thead>
								<tr>
									<th>Id</th>
									<th>Title</th>
									<th>Description</th>
									<th>Name</th>
									<th>Photo/Icon</th>
									<th>Page Name</th>
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
								<?php
								foreach($pages as $page){
									?>
									<tr>
										<td><?php echo $page->id; ?></td>
										<td><?php echo $page->title; ?></td>
										<td><?php echo $page->description; ?></td>
										<td><?php echo $page->name; ?></td>
										<td><img src="<?php echo base_url(); ?>upload/images/<?php echo $page->photo; ?>" width="100px" height="80px"/></td>
										<td><?php echo $page->page_name; ?></td>
										<td><a href="<?php echo base_url(); ?>/page/edit/<?php echo $page->id; ?>" class="btn btn-success"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="<?php echo base_url(); ?>page/delete/<?php echo $page->id; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</a></td>
									</tr>
									<?php
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- End Row-->
	</div>
	<!-- End container-fluid-->

</div><!--End content-wrapper

<?php
$this->load->view('backend/layout/footer');
?>
