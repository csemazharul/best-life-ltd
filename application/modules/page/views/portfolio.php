<?php
$this->load->view('frontend/layout/header');
?>

<!-- BANNER -->
<div class="section banner-page" data-background="<?php echo base_url(); ?>ui/frontend/images/dummy-img-1920x300.jpg">
	<div class="content-wrap pos-relative">
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<div class="title-page">PORTFOLIO</div>
		</div>
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb ">
					<li class="breadcrumb-item"><a href="index.html">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Portfolio</li>
				</ol>
			</nav>
		</div>
	</div>
</div>

<!-- CONTENT -->
<div id="projects">
	<div class="content-wrap">
		<div class="container">

			<div class="row mb-5">
				<div class="col-sm-12 col-md-12">
					<nav class="navfilter">
						<ul class="portfolio_filter">
							<li><a href="" data-filter=".design" class="active">Design</a></li>
							<li><a href="" data-filter=".web" class="">Web</a></li>
							<li><a href="" data-filter=".photo" class="">Photo</a></li>
						</ul>
					</nav>
				</div>
			</div>

			<div class="row popup-gallery gutter-5 grid-v1">
				<div class="grid-sizer-v1"></div>
				<div class="gutter-sizer-v1"></div>

				<!-- Item 1 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 design mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #1">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Notebook Mockup</div>
							<div class="category">Design</div>

						</div>
					</div>
				</div>
				<!-- Item 2 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 design mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #2">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Paper Hot Cup</div>
							<div class="category">Design</div>

						</div>
					</div>
				</div>
				<!-- Item 3 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 design mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #3">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Jamu Bottle</div>
							<div class="category">Design</div>

						</div>
					</div>
				</div>
				<!-- Item 4 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 design mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #4">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Papper Bag</div>
							<div class="category">Design</div>

						</div>
					</div>
				</div>
				<!-- Item 5 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 design mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #5">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Hanging T-Shirt</div>
							<div class="category">Design</div>

						</div>
					</div>
				</div>
				<!-- Item 6 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 design mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #6">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Notebook Mockup Resource</div>
							<div class="category">Design</div>

						</div>
					</div>
				</div>

				<!-- Item 7 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 web mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #7">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">App Screens</div>
							<div class="category">Web</div>

						</div>
					</div>
				</div>
				<!-- Item 8 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 web mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #8">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">App Music</div>
							<div class="category">Web</div>

						</div>
					</div>
				</div>
				<!-- Item 9 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 web mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #9">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Website Mockup Resource</div>
							<div class="category">Web</div>

						</div>
					</div>
				</div>
				<!-- Item 10 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 web mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #10">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">App Presentation</div>
							<div class="category">Web</div>

						</div>
					</div>
				</div>
				<!-- Item 11 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 web mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #11">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Commerce Web</div>
							<div class="category">Web</div>

						</div>
					</div>
				</div>
				<!-- Item 12 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 web mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #12">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Education Web</div>
							<div class="category">Web</div>

						</div>
					</div>
				</div>
				<!-- Item 13 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 photo mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #13">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Portrait Photography</div>
							<div class="category">Photo</div>

						</div>
					</div>
				</div>
				<!-- Item 14 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 photo mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #14">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Portrait Shoot</div>
							<div class="category">Photo</div>

						</div>
					</div>
				</div>
				<!-- Item 15 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 photo mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #15">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Marina Laslasan</div>
							<div class="category">Photo</div>

						</div>
					</div>
				</div>
				<!-- Item 16 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 photo mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #16">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Charly Vanhoten</div>
							<div class="category">Photo</div>

						</div>
					</div>
				</div>
				<!-- Item 17 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 photo mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #17">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Anastasia Sheva</div>
							<div class="category">Photo</div>

						</div>
					</div>
				</div>
				<!-- Item 18 -->
				<div class="col-sm-12 col-md-6 col-lg-4 grid-item-v1 photo mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" title="Gallery #18">
								<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x500.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Senior Picture</div>
							<div class="category">Photo</div>

						</div>
					</div>
				</div>


			</div>


		</div>
	</div>
</div>

<!-- CTA -->
<div class="section bg-primary">
	<div class="content-wrap py-5">
		<div class="container">

			<div class="row align-items-center">
				<div class="col-sm-12 col-md-12">
					<div class="cta-1">
						<div class="body-text text-white mb-3">
							<h3 class="my-1">Grow Up Your Business With Coxe</h3>
							<p class="uk18 mb-0">We provide high standar clean website for your business solutions</p>
						</div>
						<div class="body-action mt-3">
							<a href="#" class="btn btn-secondary">PURCHASE NOW</a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<?php
$this->load->view('frontend/layout/footer');
?>
